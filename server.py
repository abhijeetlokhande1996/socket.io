from aiohttp import web
import socketio
import json
sio = socketio.AsyncServer()
app = web.Application()
sio.attach(app)
client_map = {}


@sio.event
async def connect(sid, environ):
    print("Connect: ", sid)
    await sio.emit("from_server_message", {
        "message": f"Hello, your sid is {sid}"
    }, room=sid)


@sio.event
def register_client(sid, data):
    client_map[data["key"]] = sid


@sio.event
async def send_msg_to(sid, data):
    await sio.emit("from_server_message", data, room=client_map[data["to"]])


@sio.event
async def from_client_message(sid, data):
    print(f"Message from Client {sid}", data)
    # await sio.emit("my_message", room=sid)


@sio.event
def disconnect(sid):
    print("disconnect: ", sid)


if __name__ == "__main__":
    web.run_app(app)
