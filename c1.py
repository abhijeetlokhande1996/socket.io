import socketio
import time
sio = socketio.Client()


@sio.event
def connect():
    print("Connection established")
    sio.emit("register_client", {
        "key": "abc"
    })
    # sio.emit("send_msg_to", {"to": "xyz", "msg": "Hello from c1"})
    sio.sleep(3)
    while True:
        sio.emit("send_msg_to", {
            "to": "xyz",
            "msg": "Hello from abc!!!"
        })
        sio.sleep(2)


@sio.event
def from_server_message(data):
    print("Message recieved with: ", data)


@sio.event
def disconnect():
    print("disconnected from server")


sio.connect("http://localhost:8080")
sio.wait()
