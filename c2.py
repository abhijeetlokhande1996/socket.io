import socketio
import time
sio = socketio.Client()


@sio.event
def connect():
    print("Connection established")
    sio.emit("register_client", {
        "key": "xyz"
    })
    sio.sleep(2)
    while True:
        sio.emit("send_msg_to", {
            "to": "abc",
            "msg": "Hello from xyz!!!"
        })
        sio.sleep(2)


@sio.event
def from_server_message(data):
    print("Message recieved with: ", data)


@sio.event
def disconnect():
    print("disconnected from server")


sio.connect("http://localhost:8080")
sio.wait()
